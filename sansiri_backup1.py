import os
import cv2
from conv_ocr_v3 import cha_segment as segmentation
from keras.models import Sequential
from keras.layers import Dense, Flatten
from keras.applications import vgg16, vgg19
import numpy as np
from all_num import all_number_set


if __name__ == "__main__":
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
    shape = (64, 64, 3)
    code = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "\\", "/", "-"]

    model = Sequential()
    # vgg19_model = vgg19.VGG19()
    vgg16_model = vgg16.VGG16(include_top=False, input_shape=shape)  # For train in customize input shape
    # vgg16_model = vgg16.VGG16()
    for layer in vgg16_model.layers[:-1]:
        model.add(layer)

    model.add(Flatten(name='flatten'))  # For train in customize input shape
    model.add(Dense(4096, activation='relu', name='fc1'))  # For train in customize input shape
    model.add(Dense(4096, activation='relu', name='fc2'))  # For train in customize input shape
    model.add(Dense(13, activation="softmax"))

    model.load_weights("intdetector.hdf5")

    result_text = ""
    image = cv2.imread("real_world_image/fei2.png")

    data_list, img_list, row_bnd = segmentation(image)
    image_num = 0
    index_list = []
    row = []
    all_char = 0
    for sub_img in img_list:
        coordinate = []
        index = 0
        for locate in data_list[image_num]:
            y, ymax, x, xmax = locate[1], locate[3], locate[0], locate[2]
            crop = sub_img[y: ymax, x: xmax]

            resized = cv2.resize(crop, (64, 64))
            img = np.expand_dims(resized, axis=0)
            cls = model.predict_classes(img)
            conf = model.predict(img)
            if code[cls[0]] == '9' or code[cls[0]] == '-':
                if conf.tolist()[0][cls[0]]*100 > 99:
                    result_text += code[cls[0]]
                if image_num not in row:
                    row.append(image_num)
                coordinate.append(index)
            elif conf.tolist()[0][cls[0]]*100 > 99.9:
                result_text += (code[cls[0]])
                if image_num not in row:
                    row.append(image_num)
                coordinate.append(index)
            index += 1
            all_char += 1
            print(code[cls[0]], conf.tolist()[0][cls[0]]*100)
            # cv2.imshow("Frame", resized)
            # cv2.waitKey()
            # cv2.destroyAllWindows()
        if coordinate:
            index_list.append(coordinate)
        result_text += "\n"
        image_num += 1

    words = []
    for seg in result_text.replace("\n", " ").split(" "):
        if seg != '':
            words.append([seg])
    print(words, row)
    print(all_number_set(words, row, index_list, data_list, row_bnd))
    # index = 0
    # for sub_img in img_list:
    #     if index in row:
    #         cv2.rectangle(sub_img, (1420, 80), (1430, 90), (0, 0, 255), thickness=5)
    #         cv2.imshow("Yhoi", sub_img)
    #         cv2.waitKey()
    #         cv2.destroyAllWindows()
    #     index += 1

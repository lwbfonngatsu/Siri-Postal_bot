import os
import cv2
from conv_ocr_v3 import cha_segment as segmentation
from keras.models import Sequential, Model
from keras.layers import Dense, Flatten
from keras.applications import vgg16
import numpy as np
from all_num import all_number_set


def all_number_set(word_list,row_list,index_list,bnd_list,row_bnd_list):
    dic = {}
    num_list = []
    word_all = ''
    old_index = 0
    bnd = []
    check = False
    max_row = len(row_list)
    for i,row in enumerate(row_list):
        data_count = 0
        order = 1
        for word in word_list[i]:
            for e_w in word:
                data_count += 1
        for word in word_list[i]:
            count = 0
            for e_w in word:
                if(check == False):
                    word_all += e_w
                    bnd.append(bnd_list[row][index_list[i][count]])
                    old_index = bnd_list[row][index_list[i][count]]
                    check = True
                elif old_index == bnd_list[row][index_list[i][count]-1]:
                    word_all += e_w
                    old_index = bnd_list[row][index_list[i][count]]
                    bnd.append(bnd_list[row][index_list[i][count]])
                elif old_index != bnd_list[row][index_list[i][count]-1]:
                    xmin,ymin,xmax,ymax = float("inf"),float("inf"),0,0
                    for data in bnd:
                        if xmin >= data[0]:
                            xmin = data[0]
                        if xmax <= data[2]:
                            xmax = data[2]
                        if ymin >= data[1]:
                            ymin = data[1]
                        if ymax <= data[3]:
                            ymax = data[3]
                    cg_x = ((xmax + xmin)/2) + row_bnd_list[row][0]
                    cg_y = ((ymax + ymin)/2) + row_bnd_list[row][1]
                    dic['content'] = word_all
                    dic['row'] = row + 1
                    dic['position'] = (cg_x, cg_y)
                    dic['order'] = order
                    dic['max_row'] = max_row
                    num_list.append(dic.copy())
                    order += 1
                    word_all = ''
                    bnd = []
                    check = False
                    old_index = bnd_list[row][index_list[i][count]]
                    word_all += e_w
                    bnd.append(bnd_list[row][index_list[i][count]])
                if count == data_count - 1:
                    xmin, ymin, xmax, ymax = float("inf"), float("inf"), 0, 0
                    for data in bnd:
                        if xmin >= data[0]:
                            xmin = data[0]
                        if xmax <= data[2]:
                            xmax = data[2]
                        if ymin >= data[1]:
                            ymin = data[1]
                        if ymax <= data[3]:
                            ymax = data[3]
                    cg_x = ((xmax + xmin) / 2) + row_bnd_list[row][0]
                    cg_y = ((ymax + ymin) / 2) + row_bnd_list[row][1]
                    dic['content'] = word_all
                    dic['row'] = row + 1
                    dic['position'] = (cg_x, cg_y)
                    dic['order'] = order
                    dic['max_row'] = max_row
                    num_list.append(dic.copy())
                    word_all = ''
                    bnd = []
                    check = False
                count += 1
    return num_list


class Number_in_Address:
    def __init__(self, input_dict=None, input_list=None):
        self.number = input_dict['content']
        self.row = input_dict['row']
        self.pos = input_dict['position']
        self.distance = None
        self.score = 0
        self.order = input_dict['order']
        self.confidence = 0
        self.len_number = len(self.number)
        self.max_order = len(input_list)
        self.max_row = input_dict['max_row']

    def add_weight(self, slash_w=20, row_w=10, pos_w=10, order_w=10, len_w=20):
        self.slash_w = slash_w
        self.row_w = row_w
        self.pos_w = pos_w
        self.order_w = order_w
        self.len_w = len_w
        self.max_w = self.slash_w + self.row_w + self.pos_w + self.order_w + self.len_w

    def compute(self):
        if '/' in self.number:
            self.score += self.slash_w
        self.score += self.row_w - (self.row * 1)
        self.score += self.order_w - (self.order * 1)
        self.score += self.len_w - (abs(self.len_number - 6.5) * 2)

    def return_score(self):
        return (self.score / self.max_w) * 10


def get_Address(input, debug=0):
    Numbers_in_Address = []
    for i in input:
        Numbers_in_Address.append(Number_in_Address(input_list=input, input_dict=i))

        if debug == 1: print(i)

    for content in Numbers_in_Address:
        content.add_weight()
        content.compute()
        if debug == 1: print("Number is {} and Score is {}".format(content.number, content.return_score()))

    return sorted(Numbers_in_Address, key=lambda x: x.score)[-2].number


if __name__ == "__main__":
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
    shape = (64, 64, 3)
    code = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "\\", "/", "-"]

    # Intergizer Model
    intergizer_model = Sequential()
    layers = vgg16.VGG16(weights=None, input_shape=(160, 160, 3), pooling='max')
    need_layer = [layers.layers[0], layers.layers[1], layers.layers[3], layers.layers[4], layers.layers[6],
                  layers.layers[7], layers.layers[10]]
    for layer in need_layer:
        intergizer_model.add(layer)
    intergizer_model.add(Flatten(name='flatten'))
    intergizer_model.add(Dense(1024, activation='relu', name='fc1'))
    intergizer_model.add(Dense(2048, activation='relu', name='fc2'))
    intergizer_model.add(Dense(2, activation="softmax"))
    intergizer_model.load_weights("intergizer.hdf5")

    # Int detector Model
    intdetector_model = Sequential()
    vgg16_model = vgg16.VGG16(include_top=False, input_shape=shape)  # For train in customize input shape
    for layer in vgg16_model.layers[:-1]:
        intdetector_model.add(layer)
    intdetector_model.add(Flatten(name='flatten'))  # For train in customize input shape
    intdetector_model.add(Dense(4096, activation='relu', name='fc1'))  # For train in customize input shape
    intdetector_model.add(Dense(4096, activation='relu', name='fc2'))  # For train in customize input shape
    intdetector_model.add(Dense(13, activation="softmax"))
    intdetector_model.load_weights("intdetector.hdf5")

    result_text = ""
    image = cv2.imread("real_world_image/003.jpg")

    data_list, img_list, row_bnd = segmentation(image, False)
    image_num = 0
    index_list = []
    row = []
    for sub_img in img_list:
        coordinate = []
        index = 0
        for locate in data_list[image_num]:
            y, ymax, x, xmax = locate[1], locate[3], locate[0], locate[2]
            crop = sub_img[y: ymax, x: xmax]
            resized = cv2.resize(crop, (160, 160))
            resized = cv2.blur(resized, (8, 8))
            img = np.expand_dims(resized, axis=0)
            # Classify is it in our interest or not
            intergize_cls = intergizer_model.predict_classes(img)
            if intergize_cls[0] == 0:
                resized = cv2.resize(crop, (64, 64))
                img = np.expand_dims(resized, axis=0)
                # Classify numbers and / \ -
                cls = intdetector_model.predict_classes(img)
                conf = intdetector_model.predict(img)
                if code[cls[0]] == '6' or code[cls[0]] == '3' and conf.tolist()[0][cls[0]]*100 > 99.5:
                    result_text += code[cls[0]]
                    if image_num not in row:
                        row.append(image_num)
                    coordinate.append(index)
                elif code[cls[0]] == '0' and conf.tolist()[0][cls[0]]*100 > 99.7:
                    result_text += code[cls[0]]
                    if image_num not in row:
                        row.append(image_num)
                    coordinate.append(index)
                elif conf.tolist()[0][cls[0]]*100 > 99.98:
                    result_text += (code[cls[0]])
                    if image_num not in row:
                        row.append(image_num)
                    coordinate.append(index)
                index += 1
                # print(code[cls[0]], conf.tolist()[0][cls[0]]*100)
                # cv2.imshow("Frame", resized)
                # cv2.waitKey()
                # cv2.destroyAllWindows()
        if coordinate:
            index_list.append(coordinate)
        result_text += "\n"
        image_num += 1

    words = []
    for seg in result_text.replace("\n", " ").split(" "):
        if seg != '':
            words.append([seg])

    all_address = all_number_set(words, row, index_list, data_list, row_bnd)
    print(get_Address(all_address, 1))

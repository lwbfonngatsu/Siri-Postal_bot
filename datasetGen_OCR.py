from PIL import Image, ImageDraw, ImageFont
import cv2, os
import numpy as np
import xml.etree.cElementTree as ET
import random


def text2png(text, fullpath, color="#000", bgcolor="#FFF",
             fontfullpath=None, fontsize=300, leftpadding=3, rightpadding=3, width=1000, save=True):
    REPLACEMENT_CHARACTER = u'\uFFFD'
    NEWLINE_REPLACEMENT_STRING = ' ' + REPLACEMENT_CHARACTER + ' '
    font = ImageFont.load_default() if fontfullpath is None else ImageFont.truetype(fontfullpath, fontsize)
    text = text.replace('\n', NEWLINE_REPLACEMENT_STRING)
    lines = []
    line = u""
    for word in text.split():
        if word == REPLACEMENT_CHARACTER:
            lines.append(line[1:])
            line = u""
            lines.append(u"")
        elif font.getsize(line + ' ' + word)[0] <= (width - rightpadding - leftpadding):
            line += ' ' + word
        else:
            lines.append(line[1:])
            line = u""
            line += ' ' + word
    if len(line) != 0:
        lines.append(line[1:])
    line_height = font.getsize(text)[1]
    img_height = line_height * (len(lines) + 1)
    img = Image.new("RGBA", (width, img_height), bgcolor)
    draw = ImageDraw.Draw(img)
    y = 0
    for line in lines:
        draw.text((leftpadding, y), "   "+line, color, font=font)
        y += line_height
    if save:
        img.save(fullpath)
    else:
        return img
    print("Generated ", fullpath)


def vowel_gen(text, fullpath, font, fontsize=300, save=True):
    img = Image.new('RGB', (500, 500), 'white')
    font = ImageFont.truetype(font, size=fontsize, encoding='unic')
    draw = ImageDraw.Draw(img)
    draw.text((50, 50), text, (0, 0, 0), font=font)
    open_cv_image = np.array(img)
    image = open_cv_image[:, :, ::-1].copy()
    if save:
        cv2.imwrite(fullpath, image)
        print("Genarated ", fullpath)
    else:
        return image


def find_bndbox(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    retval, thresh_gray = cv2.threshold(gray, thresh=100, maxval=255, type=cv2.THRESH_BINARY)
    points = np.argwhere(thresh_gray == 0)
    points = np.fliplr(points)
    return cv2.boundingRect(points)


def crop_letter(fullpath, img=None):
    if fullpath == "" or np.any(img is not None):
        x, y, w, h = find_bndbox(img)
        crop = img[y-2:y+2 + h, x-2:x+2 + w]
        return crop
    else:
        paths = os.listdir(fullpath)
        for path in paths:
            if path.split(".")[-1] == "xml":
                continue
            image = cv2.imread(fullpath+path)
            x, y, w, h = bnd_box = find_bndbox(image)
            print("Cropping", path, bnd_box)
            crop = image[y+2:y+2 + h, x+2:x+2 + w]
            cv2.imwrite(fullpath + "/cropped_"+path, crop)


def write_xml(fullpath):
    random = cropped = False
    image = cv2.imread(fullpath)
    filenames = fullpath.split("\\")[-1]
    print("Writing ", filenames)
    if filenames.split("_")[0] == "random":
        random = True
    elif filenames.split("_")[0] == "cropped":
        cropped = True
    height, width, depth = image.shape
    x, y, w, h = find_bndbox(image)
    xmax = x + w
    ymax = y + h

    annotation_verified = ET.Element("annotation")
    folder = ET.SubElement(annotation_verified, "folder").text = "test"
    filename = ET.SubElement(annotation_verified, "filename").text = filenames
    path = ET.SubElement(annotation_verified, "path").text = fullpath
    source = ET.SubElement(annotation_verified, "source")
    ET.SubElement(source, "database").text = "Unknown"

    size = ET.SubElement(annotation_verified, "size")
    ET.SubElement(size, "width").text = str(width)
    ET.SubElement(size, "height").text = str(height)
    ET.SubElement(size, "depth").text = str(depth)

    segmented = ET.SubElement(annotation_verified, "segmented").text = "0"
    object = ET.SubElement(annotation_verified, "object")
    index = ""
    if random:
        ET.SubElement(object, "name").text = filenames.split("\\")[-1].split("_")[2]
    elif cropped:
        ET.SubElement(object, "name").text = filenames.split("\\")[-1].split("_")[2]
    else:
        ET.SubElement(object, "name").text = filenames.split("\\")[-1].split("_")[1]
    ET.SubElement(object, "pose").text = "Unspecified"
    ET.SubElement(object, "truncated").text = "0"
    ET.SubElement(object, "difficult").text = "0"
    bndbox = ET.SubElement(object, "bndbox")
    ET.SubElement(bndbox, "xmin").text = str(x)
    ET.SubElement(bndbox, "ymin").text = str(y)
    ET.SubElement(bndbox, "xmax").text = str(xmax)
    ET.SubElement(bndbox, "ymax").text = str(ymax)

    annotation = ET.ElementTree(annotation_verified)
    annotation.write(full_path + "\\" + str(filenames.split(".")[0])+".xml")


def create_random_image(char, fullpath, font, save=True):
    bg_img = np.zeros([1000, 1000, 3], dtype=np.uint8)
    bg_img.fill(255)
    fontsize = random.randrange(100, 600)
    if char in thai_vowel or char in thai_sound:
        s_img = vowel_gen(char, fullpath, font, fontsize=fontsize, save=False)
    else:
        s_img = text2png(char, fullpath, fontfullpath=font, fontsize=fontsize, save=False)
    s_img = cv2.cvtColor(np.asarray(s_img), cv2.COLOR_RGB2BGR)
    s_img = crop_letter("", img=s_img)
    x_offset = y_offset = random.randrange(100, 500)
    bg_img[y_offset:y_offset + s_img.shape[0], x_offset:x_offset + s_img.shape[1]] = s_img
    print("randomed " + fullpath)
    if save:
        cv2.imwrite(fullpath, bg_img)
    else:
        return bg_img


def image_resize(image, width=None, height=None, inter=cv2.INTER_AREA):
    dim = None
    (h, w) = image.shape[:2]
    if width is None and height is None:
        return image
    if width is None:
        r = height / float(h)
        dim = (int(w * r), height)
    else:
        r = width / float(w)
        dim = (width, int(h * r))
    resized = cv2.resize(image, dim, interpolation=inter)
    return resized


if __name__ == "__main__":
    fonts = ["font/angsana_new.ttf",
             "font/cordia_new.ttf",
             "font/THSarabunNew.ttf"]

    thai_letter = "กขฃคฅฆงจฉชซฌญฎฏฐฑฒณดตถทธนบปผฝพฟภมยรลวศษสหฬอฮ"
    letter_code = ["th1", "th2", "th3", "th4", "th5", "th6", "th7", "th8", "th9", "th10", "th11", "th12", "th13",
                   "th14", "th15", "th16", "th17", "th18", "th19", "th20", "th21", "th22", "th23", "th24", "th25",
                   "th26", "th27", "th28", "th29", "th30", "th31", "th32", "th33", "th34", "th35", "th36", "th37",
                   "th38", "th39", "th40", "th41", "th42", "th43", "th44"]

    thai_vowel = "ะาิีึืุูเแำใไั"
    vowel_code = ["thvv1", "thvv2", "thvv3", "thvv4", "thvv5", "thvv6", "thvv7", "thvv8", "thvv9", "thvv10", "thvv11",
                  "thvv12", "thvv13", "thvv14"]

    thai_sound = "่้็๋"
    sound_code = ["thsd1", "thsd2", "thsd3", "thsd4"]

    thai_number = "๐๑๒๓๔๕๖๗๘๙"
    number_code = ["thnm0", "thnm1", "thnm2", "thnm3", "thnm4", "thnm5", "thnm6", "thnm7", "thnm8", "thnm9"]

    number = "0123456789"
    nm_code = ["nm0", "nm1", "nm2", "nm3", "nm4", "nm5", "nm6", "nm7", "nm8", "nm9"]

    thai_special = "ๆ\"ฯฤฦ"
    special_code = ["thsp1", "thsp2", "thsp3", "thsp4", "thsp5"]

    special_letter = "!@#$%^&*()[]?<>|\\/-,+"
    speletter_code = ["sp1", "sp2", "sp3", "sp4", "sp5", "sp6", "sp7", "sp8", "sp9", "sp10", "sp11", "sp12", "sp13",
                      "sp14", "sp15", "sp16", "sp17", "sp18", "sp19", "sp20", "sp21"]

    # use_data = thai_letter + thai_number + number + thai_vowel + thai_sound + thai_special + special_letter
    # use_code = letter_code + number_code + nm_code + vowel_code + sound_code + special_code + speletter_code

    use_data = number + "\\/-"
    use_code = nm_code + ["sp17", "sp18", "sp19"]

    use_data = use_data
    use_code = use_code

    max_train_each = 800
    max_valid_each = 200

    # Train
    path = "demo_dataset/train/"
    index = 0
    for char in use_data:
        for i in range(max_train_each):
            to_path = path + use_code[index] + "/" + "train_" + use_code[index] + str(index) + str(i) + ".png"
            image = create_random_image(char, to_path, fonts[int(i/267)], save=False)
            cv2.imwrite(to_path, crop_letter("", img=image))
        index += 1

    # valid
    path = "demo_dataset/valid/"
    index = 0
    for char in use_data:
        for i in range(max_valid_each):
            to_path = path + use_code[index] + "/" + "valid_" + use_code[index] + str(index) + str(i) + ".png"
            image = create_random_image(char, to_path, fonts[int(i / 67)], save=False)
            cv2.imwrite(to_path, crop_letter("", img=image))
        index += 1

    # Test
    path = "demo_dataset/test/"
    index = 0
    for char in use_data:
        for i in range(max_valid_each):
            to_path = path + use_code[index] + "/" + "test_" + use_code[index] + str(index) + str(i) + ".png"
            image = create_random_image(char, to_path, fonts[int(i / 67)], save=False)
            cv2.imwrite(to_path, crop_letter("", img=image))
        index += 1

    # Generate train data
    full_path = "C:\\tensorflow1\\models\\research\\object_detection\\images\\train\\"
    # for font in fonts:
    #     count = 0
    #     font_name = font.split("/")[-1].split(".")[0]
    #     for char in use_data:
    #         if char in thai_vowel or char in thai_sound:
    #             for x in range(20):
    #                 vowel_gen(char, full_path + str(x) + "_" + use_code[count] + "_" + font_name + '.png', font)
    #         else:
    #             for x in range(20):
    #                 text2png(char, full_path + str(x) + "_" + use_code[count] + "_" + font_name + '.png',
    #                          fontfullpath=font)
    #         count += 1
    # crop_letter(full_path)
    # for font in fonts:
    #     font_name = font.split("/")[-1].split(".")[0]
    #     files = os.listdir(full_path)
    #     for file in files:
    #         if file.split(".")[1] == "png":
    #             write_xml(full_path + "\\" + file)
    # for font in fonts:
    #     count = 0
    #     font_name = font.split("/")[-1].split(".")[0]
    #     for char in use_data:
    #         for i in range(50):
    #             create_random_image(char, full_path + "random_" + str(i) + "_" + use_code[count] + "_" + font_name +
    #                                 ".png", font)
    #             write_xml(full_path + "random_" + str(i) + "_" + use_code[count] + "_" + font_name + ".png")
    #         count += 1
    #
    # xml_file = 0
    # png_file = 0
    # paths = os.listdir(full_path)
    # for file in paths:
    #     if file.split(".")[-1] == "png":
    #         png_file += 1
    #     elif file.split(".")[-1] == "xml":
    #         xml_file += 1
    # if xml_file == png_file:
    #     print("\n\n\n\nThe number are correct!")
    # else:
    #     print("\n\n\n\nSomething wrong!")
    #
    # # Generate test data
    # full_path = "C:\\tensorflow1\\models\\research\\object_detection\\images\\test\\"
    #
    # for font in fonts:
    #     count = 0
    #     font_name = font.split("/")[-1].split(".")[0]
    #     for char in use_data:
    #         for i in range(30):
    #             create_random_image(char, full_path + "random_" + str(i) + "_" + use_code[count] + "_" + font_name +
    #                                 ".png", font)
    #             write_xml(full_path + "random_" + str(i) + "_" + use_code[count] + "_" + font_name + ".png")
    #         count += 1
    #
    # xml_file = 0
    # png_file = 0
    # paths = os.listdir(full_path)
    # for file in paths:
    #     if file.split(".")[-1] == "png":
    #         png_file += 1
    #     elif file.split(".")[-1] == "xml":
    #         xml_file += 1
    # if xml_file == png_file:
    #     print("\n\n\n\nThe number are correct!")
    # else:
    #     print("\n\n\n\nSomething wrong!")



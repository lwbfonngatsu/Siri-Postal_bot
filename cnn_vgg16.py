from keras.models import Sequential
from keras.layers import Dense, Flatten, Input
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import Adam
from keras.applications import vgg16
from keras.callbacks import ModelCheckpoint, EarlyStopping

x, y = 64, 64
train_datas = 10400
valid_datas = 5200
batch = 300
mdl = "vgg16"

model = Sequential()
vgg16_model = vgg16.VGG16(include_top=False, input_shape=(x, y, 3)) # For train in customize input shape
for layer in vgg16_model.layers[:-1]:
    model.add(layer)

for layer in model.layers:
    layer.trainable = False

model.add(Flatten(name='flatten')) # For train in customize input shape
model.add(Dense(4096, activation='relu', name='fc1')) # For train in customize input shape
model.add(Dense(4096, activation='relu', name='fc2')) # For train in customize input shape

model.add(Dense(13, activation="softmax"))
model.summary()

train_path = "demo_dataset/train"
valid_path = "demo_dataset/valid"

train_batch = ImageDataGenerator().flow_from_directory(train_path,
                                                       target_size=(x, y),
                                                       classes=["nm0", "nm1", "nm2", "nm3", "nm4", "nm5", "nm6", "nm7",
                                                                "nm8", "nm9", "sp17", "sp18", "sp19"],
                                                       batch_size=batch)
valid_batch = ImageDataGenerator().flow_from_directory(valid_path,
                                                       target_size=(x, y),
                                                       classes=["nm0", "nm1", "nm2", "nm3", "nm4", "nm5", "nm6", "nm7",
                                                                "nm8", "nm9", "sp17", "sp18", "sp19"],
                                                       batch_size=batch)

model.compile(Adam(lr=.0001), loss='categorical_crossentropy', metrics=['accuracy'])
filepath = mdl + "_" + str(x) + "x" + str(y) + "_" + str(batch) + ".hdf5"
checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=True, mode='max')
# early_stop = EarlyStopping(monitor='val_acc', min_delta=0, patience=3, verbose=1, mode='auto', baseline=None)

callbacks_list = [checkpoint]
model.fit_generator(train_batch, steps_per_epoch=train_datas/batch,
                    validation_data=valid_batch, validation_steps=valid_datas/batch, epochs=300, verbose=2,
                    callbacks=callbacks_list)

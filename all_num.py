def all_number_set(word_list,row_list,index_list,bnd_list,row_bnd_list):
    dic = {}
    num_list = []
    word_all = ''
    old_index = 0
    bnd = []
    check = False
    max_row = len(row_list)
    for i,row in enumerate(row_list):
        data_count = 0
        order = 1
        for word in word_list[i]:
            for e_w in word:
                data_count += 1
        for word in word_list[i]:
            count = 0
            for e_w in word:
                if(check == False):
                    word_all += e_w
                    bnd.append(bnd_list[row][index_list[i][count]])
                    old_index = bnd_list[row][index_list[i][count]]
                    check = True
                elif old_index == bnd_list[row][index_list[i][count]-1]:
                    word_all += e_w
                    old_index = bnd_list[row][index_list[i][count]]
                    bnd.append(bnd_list[row][index_list[i][count]])
                elif old_index != bnd_list[row][index_list[i][count]-1]:
                    xmin,ymin,xmax,ymax = float("inf"),float("inf"),0,0
                    for data in bnd:
                        if xmin >= data[0]:
                            xmin = data[0]
                        if xmax <= data[2]:
                            xmax = data[2]
                        if ymin >= data[1]:
                            ymin = data[1]
                        if ymax <= data[3]:
                            ymax = data[3]
                    cg_x = ((xmax + xmin)/2) + row_bnd_list[row][0]
                    cg_y = ((ymax + ymin)/2) + row_bnd_list[row][1]
                    dic['content'] = word_all
                    dic['row'] = row
                    dic['position'] = (cg_x, cg_y)
                    dic['order'] = order
                    num_list.append(dic.copy())
                    order += 1
                    word_all = ''
                    bnd = []
                    check = False
                    old_index = bnd_list[row][index_list[i][count]]
                    word_all += e_w
                    bnd.append(bnd_list[row][index_list[i][count]])
                if count == data_count - 1:
                    xmin, ymin, xmax, ymax = float("inf"), float("inf"), 0, 0
                    for data in bnd:
                        if xmin >= data[0]:
                            xmin = data[0]
                        if xmax <= data[2]:
                            xmax = data[2]
                        if ymin >= data[1]:
                            ymin = data[1]
                        if ymax <= data[3]:
                            ymax = data[3]
                    cg_x = ((xmax + xmin) / 2) + row_bnd_list[row][0]
                    cg_y = ((ymax + ymin) / 2) + row_bnd_list[row][1]
                    dic['content'] = word_all
                    dic['row'] = row
                    dic['position'] = (cg_x, cg_y)
                    dic['order'] = order
                    num_list.append(dic.copy())
                    word_all = ''
                    bnd = []
                    check = False
                count += 1
    num_list.append(max_row)
    return num_list

if  __name__ == "__main__":
    word_list = [['123'],['4/6'],['788']]
    #test = ['364/5/2', '77701\\', '0/10890']
    row_list = [0,1,2]
    index_list = [[0,1,2],[0,1,2],[0,1,2,3]]
    bnd_list = [[(5,3,10,11),(4,1,5,5),(9,2,11,7)],[(1,6,7,8),(5,1,6,4),(4,6,7,8)],[(2,6,4,8),(1,3,5,7),(3,2,6,4),(9,6,15,9)]]
    row_bnd_list = [(23, 133, 996, 183), (22, 207, 417, 247),(22,305,845,355)]
    #word_list = [['123/4678910']]
    #row_list = [0]
    #index_list = [[0, 1, 3, 4, 5, 6, 7,8,10,11,12]]
    #bnd_list = [[(5, 3, 10, 11), (4, 1, 5, 5), (9, 2, 11, 7), (1, 6, 7, 8), (5, 1, 6, 4), (4, 6, 7, 8),(2, 6, 4, 8), (1, 3, 5, 7), (3, 2, 6, 4), (11,4,13,9),(7,8,13,15),(1,5,11,9),(5,9,8,14)]]
    #row_bnd_list = [(23, 133, 996, 183)]
    print(all_number_set(word_list,row_list,index_list,bnd_list,row_bnd_list))